﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Weather_app
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            
            RootObject myWeather = await OpenWeatherData.GetWeather();
            for (int i = 0; i < myWeather.list.Count; i++)
            {
                String icon = String.Format("http://openweathermap.org/img/w/{0}.png", myWeather.list[i].weather[0].icon);
                String des = myWeather.list[i].weather[0].description;
                int clo = myWeather.list[i].clouds.all;
                ResultImage.Source = new BitmapImage(new Uri(icon, UriKind.Absolute));
                
        }
    }
}
