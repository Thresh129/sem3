﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using WeatherAccu.Model;
using System.Text.RegularExpressions;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WeatherAccu
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<WeatherJSON> WeatherEachHours;
        ObservableCollection<DailyForecast> WeatherEachDays;
        public MainPage()
        {
            this.InitializeComponent();
            WeatherEachHours = new ObservableCollection<WeatherJSON>();
                InitJSON();
            WeatherEachDays = new ObservableCollection<DailyForecast>();
            InitEachDay();
        }
        public async void InitJSON()
        {
            var url = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/353412?apikey=93Qg780lHwYM4SO58n7DFPLqHg4oKADn&amp;language=vi-vn&amp;metric=true";
            var list = await WeatherJSON.GetJSON(url) as List<WeatherJSON>;
            list.ForEach(it =>
            {
            var match = Regex.Matches(it.DateTime, "T(?<time>\\d+):")[0].Groups["time"].Value;
            if(int.Parse(match) > 12)
            {
                match = (int.Parse(match)) - 12) + "PM";
                }
            else
            {
                match += "PM";
            }
                            
            });
        public async void InitEachDay()
        {
            var urlFiveDay = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/353412?apikey=tb93Qg780lHwYM4SO58n7DFPLqHg4oKADn&amp;language=vi-vn&amp;metric=true";

            var obj = await WeatherEachDay.GetWeatherEachDay(urlFiveDay) as WeatherEachDay;
        }
    }
}
