﻿using System;

namespace ConsoleApp1
{
    class Phone
    {
        protected string memberphonename { get; set; }
        protected string memberphonetype { get; set; }
        protected float memberphoneprice { get; set; }

        public Phone()
        {

        }

        public Phone(string memberphoneame, string memberphonetype, float memberphoneprice)
        {
            this.memberphonename = memberphoneame;
            this.memberphonetype = "Mobile";
            this.memberphoneprice = memberphoneprice;
        }

        public void input()
        {
            Console.WriteLine(" Phone name :");
            memberphonename = Console.ReadLine();
            Console.WriteLine(" Phone type :");
            memberphonetype = Console.ReadLine();
            Console.WriteLine(" Phone price :");
            memberphoneprice = float.Parse(Console.ReadLine());

        }


        public void display()
        {
            Console.WriteLine($"Phone Information: -> phone = {this.memberphonename}, type = {this.memberphonetype}, price = {this.memberphoneprice}");
        }
    }
}
